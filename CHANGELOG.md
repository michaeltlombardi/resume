# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.4.0] - 2018-12-10

### Added

- Section for PSPowerHour

### Changed

- Skills level to be `Expert` instead of `Master` due to the connotations of the latter.
- Skill level for Solutions Architecture from beginner to intermediate due to changing expertise.

### Fixed

- Documentation skill keywords no longer break across lines.

## [1.3.0] - 2018-12-05

### Added

- Section for employment at Puppet.
- End date for employment at Maritz.
- Additional interests and languages.

## [1.2.0] - 2017-06-09

### Added

- Added email to contact information.
  Note that there's a space in the email when copying - this was done to allow for a clean line break.

## [1.1.0] - 2017-06-09

### Added

- Added ChatterOps to the volunteer section.
  Will probably iterate on this later as the website and other media come online.

## [1.0.1] - 2017-03-25

### Modified

- CI to be able to build properly in the pipeline

## [1.0.0] - 2017-03-25

### Added

- Initial resume upload
- Standard metadocuments
- First pass at CI