# Helping Out
If you notice some error, inconsistency, or rendering problem with my resume, please [let me know](https://gitlab.com/michaeltlombardi/resume/issues/new) and I'll make sure to correct it.
If you're extra-super-kind and submit a merge request for the problem, I'll buy you a drink or a snack of your choice. 😊