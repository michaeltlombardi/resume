# Resume
[![build status](https://gitlab.com/michaeltlombardi/resume/badges/master/build.svg)](https://gitlab.com/michaeltlombardi/resume/commits/master)

This is my personal resume; I'm using the [JSON-Resume](https://jsonresume.org/) project and the [Caffeine theme](https://github.com/kelyvin/jsonresume-theme-caffeine).

I'm using GitLab's CI to automatically update the HTML version of this resume.